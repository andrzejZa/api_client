package com.az.apiclient.ui;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.apiclient.EditMovieActivity;
import com.az.apiclient.MainActivity;
import com.az.apiclient.R;
import com.az.apiclient.model.MovieModel;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private Context context;
    List<MovieModel> movieModels;
    public MovieAdapter(List<MovieModel> movieModels, Context context) {
        this.movieModels = movieModels;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView itemImage;
        public TextView itemTitle;
        public TextView itemDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            itemTitle =
                    (TextView) itemView.findViewById(R.id.item_title);
            itemDetail =
                    (TextView) itemView.findViewById(R.id.item_detail);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    MovieModel e = movieModels.get(position);
                    Intent intent = new Intent(context, EditMovieActivity.class);
                    intent.putExtra("ID",e.id);
                    intent.putExtra("TITLE",e.title);
                    intent.putExtra("DESCRIPTION",e.description);
                    ((FragmentActivity)context).startActivityForResult(intent,0);
                }
            });

        }
    }

    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout,parent,false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(MovieAdapter.ViewHolder holder, int position) {
        MovieModel e = movieModels.get(position);
        holder.itemTitle.setText(e.title);
        holder.itemDetail.setText(e.description);
        // holder.itemDetail.setText(String.format("%3.50s", e.Description));

        // holder.itemImage.setImageResource(holder.itemView.getContext().getResources().getIdentifier(e.Image, "drawable", packageName)); }
    }

    @Override
    public int getItemCount() {
        return movieModels.size();
    }
}
