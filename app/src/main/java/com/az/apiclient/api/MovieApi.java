package com.az.apiclient.api;

import com.az.apiclient.responce.MovieJsonItem;
import com.az.apiclient.responce.MoviesJsonResponce;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApi {

    @GET("movies")
    Single<MoviesJsonResponce> moviesResponce();

    @POST("addMovie")
    @FormUrlEncoded
    Call<MovieJsonItem> add( @Field("data") String data);

//@POST("addMovie")
//Call<MovieJsonItem> add( @Query("data") String data);
//

    @DELETE("deleteMovie/{input}")
    Single<String> deleteMovie(@Path("input") String input);

    @POST("movies/{input}")
    @FormUrlEncoded
    Single<MovieJsonItem> editMovie(@Path("input") String input, @Field("data") String data);
}
