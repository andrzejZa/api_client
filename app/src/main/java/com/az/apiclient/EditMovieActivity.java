package com.az.apiclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.az.apiclient.domain.MovieData;
import com.az.apiclient.model.MovieModel;
import com.az.apiclient.responce.MovieJsonItem;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMovieActivity extends AppCompatActivity {
    int Id;
    String title;
    String description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_movie);
        Id =getIntent().getIntExtra("ID",0);
        title =getIntent().getStringExtra("TITLE");
        description= getIntent().getStringExtra("DESCRIPTION");

       final EditText titleTxt = findViewById(R.id.edit_title);
        final EditText descriptionTxt = findViewById(R.id.edit_description);

        Button DeleteBtn = findViewById(R.id.button_delete);
        if (Id < 1){
            DeleteBtn.setVisibility(View.INVISIBLE);
        } else {
            titleTxt.setText(title);
            descriptionTxt.setText(description);
            DeleteBtn.setVisibility(View.VISIBLE);
            DeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //delete
                    new MovieData().deleteMovie(Id)
                            .subscribe(new Action() {
                                @Override
                                public void run() throws Exception {
                                    Intent returnIntent = new Intent();

                                    setResult(EditMovieActivity.this.RESULT_OK,returnIntent);

                                    finish();
                                }
                            });
                }
            });
        }

        Button SaveBtn =findViewById(R.id.button_save);
        SaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Id==0) {
                    JSONObject o = new JSONObject();
                    try {
                        o.put("title",titleTxt.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new MovieData().addMovie(new MovieModel(titleTxt.getText().toString(),descriptionTxt.getText().toString()))
                            .enqueue(new Callback<MovieJsonItem>() {
                                @Override
                                public void onResponse(Call<MovieJsonItem> call, Response<MovieJsonItem> response) {
                                    Intent returnIntent = new Intent();

                                    setResult(EditMovieActivity.this.RESULT_OK,returnIntent);
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<MovieJsonItem> call, Throwable t) {
                                    Toast.makeText(EditMovieActivity.this,"Failed add new movie",500).show();
                                }
                            });

                } else {
                    new MovieData().editMovie(Id,new MovieModel(titleTxt.getText().toString(),descriptionTxt.getText().toString()))
                            .subscribe(new Action() {
                                @Override
                                public void run() throws Exception {
                                    Intent returnIntent = new Intent();

                                    setResult(EditMovieActivity.this.RESULT_OK,returnIntent);
                                    finish();
                                }
                            });
                }
            }
        });
    }
}
