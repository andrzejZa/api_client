package com.az.apiclient.domain;

import android.content.Intent;

import com.az.apiclient.api.MovieApi;
import com.az.apiclient.data.ConnectionService;
import com.az.apiclient.model.MovieModel;
import com.az.apiclient.responce.MovieJsonItem;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Observable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.reactivex.internal.operators.single.SingleInternalHelper.toObservable;

public class MovieData {

    public Single<List<MovieModel>> loadData(){
        return ConnectionService.Get(MovieApi.class)
                .moviesResponce().toObservable()
                .flatMapIterable(item -> item.getMovies())
                .map(item -> new MovieModel(item))
                .toList()
                .subscribeOn(Schedulers.io());
    }

    public Call<MovieJsonItem> addMovie(MovieModel model){

        return ConnectionService.Get(MovieApi.class)
                .add(new Gson().toJson(model));


    }

    public io.reactivex.Completable deleteMovie (Integer id){
       return ConnectionService.Get(MovieApi.class)
                .deleteMovie(id.toString())
                .toCompletable()
                .subscribeOn(Schedulers.io());


    }

    public io.reactivex.Completable editMovie (Integer id, MovieModel model){
        return ConnectionService.Get(MovieApi.class)
                .editMovie(id.toString(), new Gson().toJson(model))
                .toCompletable()
                .subscribeOn(Schedulers.io());
    }

}
