package com.az.apiclient.model;

import com.az.apiclient.responce.MovieJsonItem;

public class MovieModel {
    public String title;
    public Integer id;
    public String description;

    public MovieModel(String title, String description) {
        this.title = title;
        this.description = description;

    }

    public MovieModel(MovieJsonItem item) {
        this.title = item.title;
        this.id = item.id;
        this.description = item.description;

    }
}
