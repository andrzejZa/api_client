package com.az.apiclient.data;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectionService {
    private OkHttpClient httpClient;
    private Retrofit retrofit;
    private Long connectionTimeout= new Long(1);
    private Long writeTimeout = new Long(1);
    private Long readTimeout = new Long(1);
    private static ConnectionService instance;


    public static <T> T Get(Class<T> type){
        if (instance == null)
            instance = new ConnectionService();
        return instance.retrofit.create(type);

    }

    private ConnectionService(){
            httpClient = getOkHttpClient();
            retrofit = getRetrofit();

    }
    private Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory( GsonConverterFactory.create())
               // .baseUrl(BuildConfig.API_URL) // "https://api.flickr.com/services/"
                .baseUrl("http://10.0.2.2:8081/")
                .build();


    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(connectionTimeout, TimeUnit.MINUTES)
                .writeTimeout(writeTimeout, TimeUnit.MINUTES)
                .readTimeout(readTimeout, TimeUnit.MINUTES);

        return httpClientBuilder.build();
    }
}
